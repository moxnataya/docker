# Развертывание Docker-образа с приложением

## Введение

**Docker** помогает разворачивать приложения и управлять ими на разных серверах и устройствах. Docker похож на виртуальную машину, но более эффективен и легковесен.

У Docker есть две основные сущности — Docker-образ и Docker-контейнер. 

* **Docker-образ** — это образ вашего приложения со всеми необходимыми зависимостями для работы. 
* **Docker-контейнер** — экземпляр образа с вашим приложением. Именно в контейнере происходит запуск приложения. Для одного образа может быть много контейнеров.

В этой инструкции показан порядок работы с Docker Desktop для операционной системы Windows.

<div style="background-color: #EBF5FB; padding: 10px; border: 1px solid #EBF5FB; width: 95%;">
    Перед началом работы установите <a href="https://www.docker.com/products/docker-desktop/" target="_blank">Docker Desktop</a> и создайте учетную запись на сайте <a href="https://hub.docker.com/" target="_blank">Docker Hub</a>.
</div>

## Порядок действий

Чтобы развернуть приложение с помощью Docker, выполните три шага:

* [Шаг 1. Создайте проект](#1)
* [Шаг 2. Соберите Docker-образ](#2-docker-)
* [Шаг 3. Запустите Docker-контейнер](#3-docker-)



## Шаг 1. Создание проекта

Для работы с Docker ваше приложение нужно подготовить: проект должен иметь определенную структуру папок
и необходимые конфигурационные файлы.

Выполните следующие действия:

* [Создайте структуру папок](#1_1)
* [Создайте файл application.py](#2-applicationpy)
* [Создайте Dockerfile](#3-c-dockerfile)

### 1. Создание структуры папок

Чтобы создать структуру папок проекта, в командной строке выполните следующую команду:

```bash
mkdir quickstart_docker\application quickstart_docker\docker\application
# Создает структуру папок для Docker-приложения
```

Чтобы проверить правильность структуры папок проекта, в командной строке перейдите в папку **quickstart_docker** и выполните следующую команду:

```bash
tree quickstart_docker
```

Структура проекта должна выглядеть так:

![Структура папок проекта](images/strukturapapok.svg)

### 2. Создание файла application.py

В командной сроке перейдите в папку **application** и создайте файл **application.py**:

```bash
echo > application.py
```
**Важно:** Если в открывшемся файле есть текст `Echo is on`, удалите его.

Добавьте в файл код, который приведен ниже. Этот код будет использоваться для сборки Docker-образа.

```python
import http.server
import socketserver

PORT = 8800
# Задает порт, на котором будет запущен HTTP-сервер


Handler = http.server.SimpleHTTPRequestHandler
# Задает обработчик запросов
# http.server.SimpleHTTPRequestHandler — класс в Python, который позволяет быстро создать простой веб-сервер

httpd = socketserver.TCPServer(("", PORT), Handler)
# Создает и начинать слушать HTTP-сокет, а также передает запросы обработчику


print(f"serving at port {PORT}")
# Выводит сообщение о запуске сервера

httpd.serve_forever()
# Обрабатывает запросы, пока сервер не будет остановлен
```

### 3. Cоздание файла Dockerfile 

В командной строке перейдите в папку **docker**. В ней создайте файл с названием **Dockerfile** с помощью следующей команды:

```bash
echo > Dockerfile
```
**Важно**: Если в открывшемся файле есть текст `Echo is on`, удалите его.

Добавьте в **Dockerfile** следующий код:

```bash

FROM python:3.9
# Скачивает с Docker Hub базовый образ Python

WORKDIR /app
# Создает рабочую папку для образа в контейнере

COPY ../application /app
# Копирует содержимое папки application в рабочую директорию

EXPOSE 8800
# Открывает порт 8800

CMD ["python3", "application.py"]
# Запускает приложение
```

## Шаг 2. Сборка Docker-образа

Предположим, что название вашего приложения — **exampleapp**. 

Чтобы собрать Docker-образ, в командной строке перейдите в корневую папку проекта и выполните следующую команду:

```bash
docker build -t exampleapp -f docker/Dockerfile .
```
<div style="background-color: #FEF9E7 ; padding: 10px; border: 1px solid #FEF9E7 ; width: 80%;">
Убедитесь, что в конце команды вы поставили точку.
</div>

Параметры команды:

<table>
  <tr>
    <td style="width: 170px;">-t exampleapp</td>
    <td>Ключ <code>-t</code> присваивает название образу. В нашем случае — <b>exampleapp</b>. </td>
  </tr>
  <tr>
    <td style="width: 170px;">-f docker/Dockerfile .</td>
    <td>Ключ <code>-f</code> указывает путь к Dockerfile, а точка (.) указывает на текущую рабочую директорию.</td>
  </tr>
</table>

Чтобы проверить, что образ создался, выполните команду:

```bash
docker images
```
Пример созданного образа:

![Созданный образ](images/imagelist.png)

Если в системе уже есть образы, последний созданный образ можно найти по тегу `latest`. Этот образ будет вверху списка.

## Шаг 3. Запуск Docker-контейнера

Чтобы запустить контейнер, выполните следующую команду:

```bash
docker run -p 8800:8800 -t -d --name exampleapp exampleapp
```

Параметры команды: 

<table>
    <tr>
        <td style="width: 170px;">-p 8800:8800</td>
        <td>Ключ <code>-p</code> перенаправляет внутренний порт хоста 8800 на внешний порт контейнера 8800.</td>
    </tr>
    <tr>
        <td style="width: 170px;">-t</td>
        <td>Ключ <code>-t</code> выделяет псевдотерминал для настройки контейнера через командную строку после его запуска.</td>
    </tr>
    <tr>
        <td style="width: 170px;">-d</td>
        <td>Ключ <code>-d</code> запускает контейнер в фоновом режиме.</td>
    </tr>
    <tr>
        <td style="width: 170px;">--name exampleapp exampleapp</td>
        <td>Команда задает имя контейнеру как <b>exampleapp</b>. Первый exampleapp — имя образа, второй — имя контейнера. В нашем случае эти имена одинаковые, но могут быть и разными.</td>
    </tr>
</table>

Чтобы посмотреть список запущенных контейнеров, выполните команду ниже:

```bash
docker ps
```

Если контейнер запущен, вы увидите следующее сообщение в командной строке:

![Созданный контейнер](images/container.png)


<div style="background-color: #EBF5FB; padding: 10px; border: 1px solid #EBF5FB; width: 95%;">
Перейдите по ссылке <a href="http://localhost:8800" target="_blank">localhost:8800</a>, чтобы запустить приложение в браузере.
</div>

Чтобы остановить контейнер, используйте следующую команду:

```bash
docker stop exampleapp
```

## Публикация Docker-образа

Docker-образ нужно опубликовать на [Docker Hub](https://hub.docker.com/). После этого вы или другой пользователь сможете скачивать образ на любое устройство и запускать приложение по этой инструкции.

**Docker Hub** — облачное хранилище для образов Docker Image. Туда можно загружать как собственные образы, так и находить образы программ, которые нужны для сборки собственных образов.


### Публикация образа на Docker Hub

Чтобы опубликовать образ на Docker Hub, задайте ему имя, которое будет отображаться на Docker Hub. Для этого выполните следующую команду:

```bash
docker tag exampleapp username/exampleapp
```

<table>
    <tr>
        <td>docker tag</td>
        <td>Команда, которая присваивает новое имя уже существующему образу</td>
    </tr>
        <tr>
        <td>exampleapp</td>
        <td>Текущее имя образа</td>
    </tr>
    <tr>
        <td>username/exampleapp</td>
        <td>Имя образа на Docker Hub, где username — ваше имя пользователя на Docker Hub, а exampleapp — название приложения.</td>
    </tr>
</table>

Чтобы загрузить образ на Docker Hub, используйте следующую команду:

```bash
docker push username/exampleapp
```

### Проверка публикации образа на Docker Hub

Зайдите в свой профиль на [Docker Hub](https://hub.docker.com/) в раздел **Repositories**, чтобы убедиться, что образ успешно загружен. При успешной загрузке вы увидите название созданного образа в списке.

<img src="images/dockerhub.png" alt="Список образов" style="width:90%; height:auto;">

## Полезные ссылки

* Больше о Docker Desktop можно почитать [сайте документации Docker](https://docs.docker.com/desktop/).
* [Подрбное руководство по Docker](downloads/Docker.pdf) от Faang School.
* [Статья на Habr](https://habr.com/ru/companies/selectel/articles/807983/) о безопасности при использования Docker.